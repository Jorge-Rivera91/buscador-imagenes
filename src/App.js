import { Field, Form, Formik } from "formik";
import { useState } from "react";
import { API_KEY, URL } from "./constant";
import './assets/css/header.css'
import './assets/css/content.css'
import './assets/css/article.css'

const App = () => {
  const [photos, setPhotos] = useState([]);
  const open = url => window.open(url);
  
  return (
    <div>
      <header>
        <Formik
          initialValues={ { search: ''} }
          onSubmit={ async (values) => {
            const response =  await fetch(`${URL}/search/photos?per_page=20&query=${values.search}`, {
              headers: {
                'Authorization': `Client-ID ${API_KEY}`,
              }
            });
            const data = await response.json();
            setPhotos(data.results);
          }}
        >
          <Form>
            <Field type='text' name='search' />
          </Form>
        </Formik>
      </header>
      <div className="container">
        <div className="center">      
          {photos.map(photo => (
            <article key={photo.id} onClick={() => open(photo.links.html)}>
              <img src={photo.urls.regular} alt={`img ${photo.alt_description}`}/>
              <p>{[photo.description, photo.alt_description].join(' - ')}</p>
            </article>
          ))}
        </div>
      </div>
    </div>
  );
}

export default App;
